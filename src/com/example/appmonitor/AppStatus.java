package com.example.appmonitor;

import android.content.pm.ApplicationInfo;
import android.graphics.drawable.Drawable;
import android.net.TrafficStats;

/**
 * Wrapper class that holds all the information of an app to be displayed.
 * @author yihuaqi
 *
 */
public class AppStatus{
	ApplicationInfo applicationInfo = null;
	String label = "Unknown";
	Drawable icon = null;
	Boolean isRunning = false;
	String openedTime = "N/A";
	String closedTime = "N/A";
	

	public ApplicationInfo getApplicationInfo() {
		return applicationInfo;
	}
	public void setApplicationInfo(ApplicationInfo applicationInfo) {
		this.applicationInfo = applicationInfo;
	}
	public String getPackageName(){
		return applicationInfo.packageName;
	}

	
	public Boolean getIsRunning() {
		return isRunning;
	}
	public void setIsRunning(Boolean isRunning) {

		this.isRunning = isRunning;
		
	}
	public String getOpenedTime() {
		return openedTime;
	}
	public void setOpenedTime(String openedTime) {
		this.openedTime = "Opened Time:"+openedTime;
	}
	public String getClosedTime() {
		return closedTime;
	}
	public void setClosedTime(String closedTime) {
		this.closedTime = "Closed Time: "+closedTime;
	}
	public String getRunningStatus() {
		if(isRunning){
			return "Status: Running";
		} else {
			return "Status: Inactive";
		}
	}
	public String getNetworkTrafficRx() {
		
		return "RX: "+TrafficStats.getUidRxBytes(applicationInfo.uid)/1000+"KB";
	}
	public String getNetworkTrafficTx() {
		
		return "TX: "+TrafficStats.getUidTxBytes(applicationInfo.uid)/1000+"KB";
	}
	public void setLabel(String label) {
		this.label = label;
		
	}
	public String getLabel(){
		return label;
	}
	public void setIcon(Drawable icon) {
		this.icon = icon;
	}
	public Drawable getIcon(){
		return icon;
	}
	
}