package com.example.appmonitor;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.util.Log;

/**
 * Service that monitors and updates the AppStatus
 * @author yihuaqi
 *
 */
public class AppMonitorService extends Service{
	

	private final static String TAG = "AppMonitorService";
	
	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		new MonitorTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		
		Log.d(TAG, "Service On Create");
		
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// TODO Auto-generated method stub
		
		return START_STICKY;
	}

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void onDestroy() {
		
		Log.d(TAG, "Service On Destroy");
		super.onDestroy();
	}

	public class MonitorTask extends AsyncTask<Void,Void, Void>{

		@Override
		protected Void doInBackground(Void... params) {
			Log.d(TAG, "Do in background");
			while(true){
				try {
					Thread.sleep(1000);
					
				} catch (InterruptedException e) {
					Log.d(TAG, "Sleep Exception");
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				AppMonitor.updateAppStatus(getApplicationContext());
				
			}
		}
		
	}

	

}
