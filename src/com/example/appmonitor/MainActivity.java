package com.example.appmonitor;


import java.security.PublicKey;
import java.util.concurrent.ThreadPoolExecutor;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.ListActivity;

import android.app.LoaderManager.LoaderCallbacks;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

public class MainActivity extends Activity{
	ListView listView;
	Button startService;
	Button stopService;
	private final String TAG = "MainActivity";
	ArrayAdapter<AppStatus> adapter;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		startService = (Button) findViewById(R.id.startService);
		stopService = (Button) findViewById(R.id.stopService);
		OnClickListener listener = new ButtonListener();
		startService.setOnClickListener(listener);
		stopService.setOnClickListener(listener);
		listView = (ListView) findViewById(R.id.listview);
		adapter = new AppStatusAdapter(this, AppMonitor.getAppStatus());
		listView.setAdapter(adapter);
		new UpdateUITask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

	}
	
	public class ButtonListener implements OnClickListener{

		@Override
		public void onClick(View v) {
			if(v==startService){
				startService(new Intent(getApplicationContext(),AppMonitorService.class));				
			}
			if(v==stopService){
				stopService(new Intent(getApplicationContext(),AppMonitorService.class));
			}
			
		}
		
	}
	
	/**
	 * AsyncTask that updates the list view
	 * @author yihuaqi
	 *
	 */
	class UpdateUITask extends AsyncTask<Void, Void, Void>{

		@Override
		protected void onProgressUpdate(Void... values) {
			adapter.notifyDataSetChanged();
		}

		@Override
		protected Void doInBackground(Void... params) {
			while(true){
				try {
					Thread.sleep(1000);
					Log.d(TAG, "publish Progress");
					publishProgress();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			
		}
		
	}
	
}

