package com.example.appmonitor;

import java.io.ObjectInput;
import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
/**
 * Customized adapter for displaying app status.
 * @author yihuaqi
 *
 */
public class AppStatusAdapter extends ArrayAdapter<AppStatus>{
	
	private static final String TAG = "AppStatusAdapter";
	public AppStatusAdapter(Context context, List<AppStatus> data){
		super(context,0,data);
	
	}
	
	private static class ViewHolder{
		ImageView icon;
		TextView label;
		TextView packageName;
		TextView runningStatus;
		TextView openedTime;
		TextView closedTime;
		TextView networkTrafficRx;
		TextView networkTrafficTx;
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		AppStatus appStatus= getItem(position);
		ViewHolder viewHolder;
		if(convertView==null){
			viewHolder = new ViewHolder();
			convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_view_row_item,parent, false);
			viewHolder.icon = (ImageView) convertView.findViewById(R.id.icon);
			viewHolder.label = (TextView) convertView.findViewById(R.id.label);
			viewHolder.packageName = (TextView) convertView.findViewById(R.id.package_name);
			viewHolder.runningStatus= (TextView) convertView.findViewById(R.id.running_status);
			viewHolder.openedTime= (TextView) convertView.findViewById(R.id.open_time);
			viewHolder.closedTime = (TextView) convertView.findViewById(R.id.close_time);
			viewHolder.networkTrafficRx = (TextView) convertView.findViewById(R.id.network_traffic_rx);
			viewHolder.networkTrafficTx = (TextView) convertView.findViewById(R.id.network_traffic_tx);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		viewHolder.icon.setImageDrawable(appStatus.getIcon());
		viewHolder.label.setText(appStatus.getLabel());
		viewHolder.packageName.setText(appStatus.getPackageName());
		viewHolder.runningStatus.setText(appStatus.getRunningStatus());
		viewHolder.openedTime.setText(appStatus.getOpenedTime());
		viewHolder.closedTime.setText(appStatus.getClosedTime());
		viewHolder.networkTrafficRx.setText(appStatus.getNetworkTrafficRx());
		viewHolder.networkTrafficTx.setText(appStatus.getNetworkTrafficTx());
		return convertView;
	}
	
	
	@Override
	public AppStatus getItem(int position) {
		return AppMonitor.getAppStatus().get(position);
	}


	@Override
	public int getCount() {
		return AppMonitor.getAppStatus().size();
		
	}
	@Override
	public int getPosition(AppStatus item) {
		return AppMonitor.getAppStatus().indexOf(item);
		
	}
	
	
	
	
}
